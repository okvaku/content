---
title: "Kotitehtävien tekeminen ja palauttaminen"
output:
  html_document:
    toc: true
    toc_float: true
    number_sections: yes
    code_folding: show
---

# Tiiviit ohjeet kotitehtävien tekemiseen ja palauttamiseen

Kotitehtävät voit tehdä luentojen tapaan joko [CSC:n palvelimella](ohjelmistoymparisto_csc.html) tai [omalla koneella](ohjelmistoymparisto_omakone.html). Laitan ohjelmistoympäristö ohjeiden mukaan aina kohtaan *"3. Kurssimateriaaliin tulleiden muutosten päivittäminen"*. Oletus on että et ole tehnyt muutoksia ja voit päivittää materiaalit seuraavilla komennoilla R:ssä. (ks. kohta )

```{r, eval=FALSE}
system("git remote add upstream git@gitlab.com:utur2016/content.git")
system("git fetch upstream")
system("git reset --hard")
system("git merge upstream/master")
```

Tämän jälkeen avaa ko. viikon kotitehtävä kuten `session2_import_tidy_kotitehtava.R` ja täytä parhaan osaamisesi mukaan kotitehväviin vastaukset. Eli jos kotitehtävät näyttävät alussa tältä

```{r eval=FALSE}
#' **Kysymys:** *Millä komennolla luot nykyisen työhakemistoon kansion `aineistot`?*
#+ vastaus11
default_answer(11)

#' **Kysymys:** *Millä komennolla luot kansioon `aineistot` tiedoston 'muistiinpanot.txt'?*
#+ vastaus12
default_answer(12)

#' **Kysymys:** *Millä komennolla kopioit kansiossa `aineistot` olevan tiedoston 'muistiinpanot.txt' samaan kansioon nimellä 'muistiinpanot.md'?*
#+ vastaus13
default_answer(13)
```

Niiden tulisi näyttää sinun tekemiesi muutosten jälkeen vaikkapa tältä

```{r eval=FALSE}
#' **Kysymys:** *Millä komennolla luot nykyisen työhakemistoon kansion `aineistot`?*
#+ vastaus11
luo.hakemisto("joku sopiva polku tähän")

#' **Kysymys:** *Millä komennolla luot kansioon `aineistot` tiedoston 'muistiinpanot.txt'?*
#+ vastaus12
luo.tiedosto("tiedoston nimi tähän")

#' **Kysymys:** *Millä komennolla kopioit kansiossa `aineistot` olevan tiedoston 'muistiinpanot.txt' samaan kansioon nimellä 'muistiinpanot.md'?*
#+ vastaus13
kopioi.tiedosto(mistä="täältä", mihin="tuonne")
```

**Kun olet saanut tehtävät valmiiksi tai lopetat niiden tekemisen tältä erää, niin päivitä muutokset gitlabiin**. Alla on kaksi vaihtoehtoa, eli voit tehdä sen joko Rstudion git-välilehdeltä graafisesti klikkaillen (metodi 1) tai ajamalla pari riviä koodia (metodi 2).

**Metodi 1 - IDE:n avulla**

1. mene Rstudiossa **git** välilehdelle ja 
    1. klikkaa tiedostot joiden muutokset haluat saada talteen ja klikkaa **Commit**
    2. kirjoita ruutuun **muutoksia kuvaava viesti sekä syy muutoksille** ja klikkaa **Commit*
    3. Sitten klikkaa ensin **Pull** ja anna ssh-salasanasi 
    4. Sitten klikkaa **Push**
4. Sulje Rstudio tai aloita jo kotitehtävät!

**Metodi 2 - koodilla**

```{r eval=FALSE}
system('git commit -am "tein kotitehtäviä ja annan tähän runsaan kuvauksen siitä mitä tein, mikä onnistui ja mikä oli vaikeaa"')
system("git pull origin master")
system("git push origin master")
```


# Vaihtoehtoinen tapa kotitehtävien palauttamiseen

Mikäli ohjelmistoympäristön asentaminen ottaa voimille ja haluat vain saada kotitehtävät nopeasti tehdyksi, niin ainakin ensimmäisellä ja toisella kerralla palauttaminen on mahdollista myös Gitlab:in ulkopuolella manuaalisesti.

Tee näin:

1. avaa R joko CSC:n palvelimilla tai omalla koneella 
2. aja seuraava komento R:ssä (vaihda oma tunnuksesi kohtaan `utu-tunnus` koodiissa)

```{r eval=FALSE}
download.file("https://gitlab.com/utur2016/content/raw/master/session2_import_tidy_kotitehtava.R",
              destfile="session2_import_tidy_kotitehtava_utu_tunnus.R")
```


3. Tee kotitehtävät, tallenna tiedoston muutokset levylle ja palauta se sähköpostilla osoitteeseen `markuskainu@gmail.com` jossa **otsikkona on ko. kotitehttävätiedoston nimi** ja **ko. tiedosto on sähköpostin liitteenä**. Siis, jos utu-tunnukseni olisi `mjkain` niin sähköposti olisi muotoa
    - vastaanottaja: `markuskainu@gmail.com`
    - otsikko:  `session2_import_tidy_kotitehtava_mjkain.R`
    - liitteenä täydennetty skripti nimellä `session2_import_tidy_kotitehtava_mjkain.R`
4. Viesti luetaan ohjelmallisesti sähköpostista, joten muodon tulee noudattaa em. ohjeita. Mitään "omin sanoin" -tyyppisiä palautuksia ei hyväksytä.

